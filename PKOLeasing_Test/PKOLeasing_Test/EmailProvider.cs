﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace PKOLeasing_Test
{
    public class EmailProvider
    {
        static string smtpAddress;
        static int portNumber;
        static bool enableSSL;


        static string emailFrom;
        static string password;

        static EmailProvider()
        {
            smtpAddress = ConfigurationSettings.AppSettings["SmtpAddress"];
            portNumber = Convert.ToInt32(ConfigurationSettings.AppSettings["PortNumber"]);
            enableSSL = Convert.ToBoolean(ConfigurationSettings.AppSettings["EnableSSL"]);
            emailFrom = ConfigurationSettings.AppSettings["EmailFrom"];
            password = ConfigurationSettings.AppSettings["Password"];
        }
        public static void SendEmails(string body, IEnumerable<string> emails)
        {
            using (SmtpClient smtp = GetSmtp())
            {
                foreach (var email in emails)
                {
                    using (MailMessage mail = new MailMessage())
                    {
                        mail.From = new MailAddress(emailFrom);
                        mail.To.Add(email);
                        mail.Subject = "PKOLeasing_zadanie " + DateTime.Now;
                        mail.Body = body;
                        mail.IsBodyHtml = enableSSL;
                        smtp.Send(mail);

                        //Tworzenie logu po poprawnym wyslaniu wiadomości
                        MessageContext.DBContext.Logs.Add(new Log(mail));
                        MessageContext.DBContext.SaveChanges();
                    }
                }

            }



        }
        private static SmtpClient GetSmtp()
        {
            return new SmtpClient
            {
                Host = smtpAddress,
                Port = portNumber,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(emailFrom, password),
                EnableSsl = enableSSL
            };
        }
    }
}