﻿using System.Runtime.Serialization;
using System.ServiceModel;

namespace MessageService
{
    [ServiceContract]
    public interface IMessageService
    {
        [OperationContract]
        MessageResponse Send(MessageRequest message);
    }

    [DataContract]
    public class MessageRequest
    {
        [DataMember] //Trzeba dodać atrybut DataMember
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public LegalForm LegalForm { get; set; }

        [DataMember]
        public Contact[] Contacts { get; set; }
    }

    [DataContract]
    public class Contact
    {
        [DataMember]
        public ContactType ContactType { get; set; }

        [DataMember]
        public string Value { get; set; }
    }

    [DataContract]
    public class MessageResponse
    {
        [DataMember]
        public ReturnCode ReturnCode { get; set; }

        [DataMember]
        public string ErrorMessage { get; set; }
    }

    [DataContract]
    public enum ReturnCode
    {
        [EnumMember]  //Trzeba dodać atrybut EnumMember
        Success = 0, //Enumy powinny miec wartosci bo przy usunieciu jednego z nich moze byc problem przy istniejacych danych

        [EnumMember]
        ValidationError = 1 ,

        [EnumMember]
        InternalError =2 
    }

    [DataContract]
    public enum LegalForm
    {
        [EnumMember]
        Person = 0,

        [EnumMember]
        Company =1
    }

    [DataContract]
    public enum ContactType
    {
        [EnumMember]
        Mobile =0,

        [EnumMember]
        Fax = 1,

        [EnumMember]
        Email =2,

        [EnumMember]
        OfficePhone =3,

        [EnumMember]
        OfficeFax =4,

        [EnumMember]
        OfficeEmail =5
    }
}
