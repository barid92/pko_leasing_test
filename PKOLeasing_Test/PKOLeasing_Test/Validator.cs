﻿using MessageService;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PKOLeasing_Test
{
    public class Validator
    {
        private List<string> _validEmails = new List<string>();
        private bool processCompleted = false;
        public List<string> ValidEmails
        {
            get
            {
                if(processCompleted)
                {
                    return _validEmails;
                }
                throw new Exception("First, start the Process functions");
            }
        }
        public MessageResponse Process(MessageRequest message)
        {
            processCompleted = false;
            var response = new MessageResponse
            {
                ReturnCode = ReturnCode.Success,
                ErrorMessage = "Ok"
            };
            _validEmails = new List<string>();

            if (message == null)
            {
                response.ReturnCode = ReturnCode.InternalError;
                response.ErrorMessage = "MessageRequest message == null";

            }
            else if (message.Contacts == null)
            {
                response.ReturnCode = ReturnCode.ValidationError;
                response.ErrorMessage = "Contacts Is Null";
            }
            else if (String.IsNullOrWhiteSpace(message.LastName))
            {
                response.ReturnCode = ReturnCode.ValidationError;
                response.ErrorMessage = "LastName Is Null Or White Space";
            }
            else
            {
                switch (message.LegalForm)
                {
                    case LegalForm.Person:
                        if (String.IsNullOrWhiteSpace(message.FirstName))
                        {
                            response.ReturnCode = ReturnCode.ValidationError;
                            response.ErrorMessage = "FirstName Is Null Or White Space";
                            break;
                        }
                        var emailsContacts = message.Contacts.Where(x => x != null && x.ContactType == ContactType.Email);
                        if (emailsContacts.Count() == 0)
                        {
                            response.ReturnCode = ReturnCode.ValidationError;
                            response.ErrorMessage = "message.Contacts.Where(x=>x.ContactType == ContactType.Email).Count() == 0";
                            break;
                        }
                        else
                        {
                            validateEmails(response, emailsContacts);

                        }
                        break;
                    case LegalForm.Company:

                        var officeEmailsContacts = message.Contacts.Where(x => x != null && x.ContactType == ContactType.OfficeEmail);
                        if (officeEmailsContacts.Count() == 0)
                        {
                            response.ReturnCode = ReturnCode.ValidationError;
                            response.ErrorMessage = "message.Contacts.Where(x => x != null && x.ContactType == ContactType.OfficeEmail).Count() == 0";
                            break;
                        }
                        else
                        {
                            validateEmails(response, officeEmailsContacts);
                        }

                        break;
                    default:
                        response.ReturnCode = ReturnCode.InternalError;
                        response.ErrorMessage = "LegalForm error";
                        break;


                }



            }



            if(response.ReturnCode != ReturnCode.Success)
            {
                _validEmails = new List<string>();
                processCompleted = false;
            }
            else
            {
                processCompleted = true;
            }

            return response;

        }

        private void validateEmails(MessageResponse response, IEnumerable<Contact> emails)
        {
            var emailAttribute = new EmailAddressAttribute();
            foreach (var contact in emails)
            {
                if (String.IsNullOrWhiteSpace(contact.Value))
                {
                    response.ReturnCode = ReturnCode.ValidationError;
                    response.ErrorMessage = String.Format("contact.Value type Email is Null Or White Space", contact.Value);
                    break;
                }
                else if (!emailAttribute.IsValid(contact.Value))
                {
                    response.ReturnCode = ReturnCode.ValidationError;
                    response.ErrorMessage = String.Format("Email: {0} is not valid", contact.Value);
                    break;
                }
                else
                {
                    _validEmails.Add(contact.Value);
                }
            }
        }
    }
}