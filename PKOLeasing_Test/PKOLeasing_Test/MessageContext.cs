﻿using MessageService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace PKOLeasing_Test
{
    public class MessageContext : DbContext
    {
        private static MessageContext dbContext = null;
        private static readonly object m_oPadLock = new object();

        public static MessageContext DBContext
        {
            get
            {
                lock (m_oPadLock)
                {
                    if (dbContext == null)
                    {
                        dbContext = new MessageContext();
                    }
                    return dbContext;
                }
            }
        }
        public MessageContext() : base(ConfigurationSettings.AppSettings["ConnectionString"])
        {

        }

        public DbSet<Log> Logs { get; set; }
    }
   
}