﻿using MessageService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace PKOLeasing_Test
{
    public class Log
    {
        public Log()
        {
        }

        public Log(MessageResponse response) : base()
        {
            this.Date = DateTime.Now;
            this.ReturnCode = response.ReturnCode;
            this.Message = response.ErrorMessage;
        }

        public Log(MailMessage mail)
        {
            this.Date = DateTime.Now;
            this.Email = mail.To.ToString();
            this.Body = mail.Body;
            this.Message = "Mail sent";
        }

        public int Id { get; set; }
        public string Email { get; set; }
        public string Body { get; set; }
        public DateTime? Date { get; set; }
        public ReturnCode? ReturnCode { get; set; }
        public string Message { get; set; }

    }
}