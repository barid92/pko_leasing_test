﻿using MessageService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace PKOLeasing_Test
{
  
    public class Service1 : IMessageService
    {
        /// UWAGA
        /// Przed uruchomieniem serwisu proszę dodać konfiguracje poczty email i połączenia z bazą danych w pliku Web.config 
        /// 
        public MessageResponse Send(MessageRequest message)
        {
 
            var validator = new Validator();

            var response = validator.Process(message);

            if(response.ReturnCode == ReturnCode.Success)
            {
                try
                {
                    var emails = validator.ValidEmails;
                    EmailProvider.SendEmails("Response success", emails);
                }
                catch (Exception ex)
                {
                    response.ErrorMessage = "EmailProvider.SendEmail('Response success', emails), " + ex.Message;
                    response.ReturnCode = ReturnCode.InternalError;
                }
            }

            MessageContext.DBContext.Logs.Add(new Log(response));
            MessageContext.DBContext.SaveChanges();
            return response;

        }
    }
}
